#!/bin/bash

kernel_info() {
  SDIRKNL="$SDIR"/kernel
  mkdir -p "$SDIRKNL"

  cat /proc/cmdline > "$SDIRKNL"/cmdline
  uname -a > "$SDIRKNL"/uname
  lsmod > "$SDIRKNL"/lsmod
}


disks_info() {
  SDIRDSK="$SDIR"/disks
  mkdir -p "$SDIRDSK"

  lsblk > "$SDIRDSK"/lsblk
  blkid > "$SDIRDSK"/blkid
}


display_info() {
  SDIRVDO="$SDIR"/video
  mkdir -p "$SDIRVDO"

for p in /sys/class/drm/card? ; do
  id=$(basename `readlink -f $p/device`)
  # now match with lspci
  name=$(lspci -mms $id | cut -d '"' -f4,6 --output-delimiter=" ")
  cardnum=$(basename $p)
  echo "$cardnum: $name" >> "$SDIRVDO"/infos
  for p in /sys/class/drm/${cardnum}-*/status; do
    con=${p%/status}
    bus=$(ls -d "$con"/i2c-* 2>/dev/null)
    echo -n "${con#*/card?-}: $(cat $p)" >> "$SDIRVDO"/infos
    [[ -n $bus ]] && echo -n " - Has i2c" >> "$SDIRVDO"/infos
    edid_size=$(cat "$con"/edid | wc -c)
        if [[ $edid_size > 0 ]] ; then
      echo -n " - Found an EDID with monitor model: " >> "$SDIRVDO"/infos
      edid-decode < "$con"/edid |grep "Display Product Name: " | sed "s/Display Product Name\: //" >> "$SDIRVDO"/infos
    fi
    cat "$con"/modes  >> "$SDIRVDO"/infos
    echo >> "$SDIRVDO"/infos
  done
done

  # List of X displays
  (cd /tmp/.X11-unix && for x in X*; do xrandr --prop -d ":${x#X}" > "$SDIRVDO"/xrandr-d${x#X}; done)
}

systemd_stuff() {
  SDIRSYSD="$SDIR"/systemd
  mkdir -p "$SDIRSYSD"

  systemd-analyze blame > "$SDIRSYSD"/blame
  systemd-analyze plot > "$SDIRSYSD"/plot.svg
}

config_files() {
  export SDIRCFG="$SDIR"/configs
  mkdir -p "$SDIRCFG"

  (cd /home/arcade/shared/configs && for f in $(find ./ -name '*.ini' -name '*.cfg') ; do install -D "$f" "$SDIRCFG"/"$f" ; done)
}


network_info() {
  SDIRSYSD="$SDIR"/network
  mkdir -p "$SDIRSYSD"

  ip a > "$SDIR"/ip
  ip route > "$SDIR"/route
}


SDIR="/tmp/ga-support/$(date +%Y%m%d%H%M%S)"
mkdir -p "$SDIR"

kernel_info
disks_info
display_info
systemd_stuff
hostnamectl > "$SDIR"/hostnamectl
systemctl > "$SDIR"/systemctl
pacman -Qqe > "$SDIR"/pacman_packages.txt
lspci -k > "$SDIR"/lspci


# log files
mkdir "$SDIR"/logs
logs=(/var/log/pacman.log)
for f in "${logs[@]}" ; do
  cp "$f" "$SDIR"/logs
done
cp /home/arcade/shared/logs/*.log "$SDIR"/logs
cp /var/log/Xorg* "$SDIR"/logs
sudo dmesg > "$SDIR"/logs/dmesg.log

# Create the archive
(cd "$SDIR" && tar caf /tmp/ga-support.tar.zst * )
# Upload the archive
#outputlink=$(curl --silent -H "Max-Days: 5" --upload-file /tmp/ga-support.tar.zst https://transfer.sh/ga-support.tar.zst)
outputlink=$(curl -F "file=@/tmp/ga-support.tar.zst" -s -w "\n" https://file.io/?expires=1w | sed -E 's+.*("link":".*").*+\1+' | cut -d '"' -f4)
rm -rf "$SDIR" /tmp/ga-support.tar.zst

echo "$outputlink"
