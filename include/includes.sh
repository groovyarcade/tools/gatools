#!/bin/bash
_USER=arcade
_GROUP=$(id -ng "$_USER")
_HOME=$(getent passwd "$_USER" | cut -d: -f6 )
MOTHER_OF_ALL="$_HOME"/shared

# the readonly stuff can produce errors when a same variable is set twice, because of includes.sh being sources multiple times in a script
exec 3>&2 2> /dev/null
readonly LOG_DIR="$MOTHER_OF_ALL"/logs/
readonly LOGFILE="$LOG_DIR"/groovy.log
#readonly LOGFILE=./groovy.log

readonly CONFIG_DIR="$_HOME"/.config
readonly CONFIG_FILE=${CONFIG_DIR}/ga.conf
readonly CONFIG_FILE_TMP=${CONFIG_DIR}/ga.tmp

readonly MAME_INI="$MOTHER_OF_ALL"/configs/mame/mame.ini
readonly SWITCHRES_CONF="$_HOME"/switchres.conf
readonly SWITCHRES_INI=/etc/switchres.ini
readonly GA_CONF_DIR="$MOTHER_OF_ALL"/configs
readonly GA_CONF="$GA_CONF_DIR"/ga.conf

readonly MAME_DIR="$MOTHER_OF_ALL"/configs/mame

readonly OS_NAME=GroovyArcade

readonly GA_VIDEO_DATA=/opt/galauncher/videodata.conf
exec 2>&3

ga_log() {
  msg_type=$1
  shift
  logdir=$(dirname "$LOGFILE")
  if [[ ! -d "$logdir" ]] ; then
    mkdir -p "$logdir"
    chmod 755 "$logdir"
  fi
  if [[ ! -e "$LOGFILE" ]] ; then
    touch "$LOGFILE"
    chmod 644 "$LOGFILE"
    chown "$_USER":"$_GROUP" "$LOGFILE"
  fi
  #echo -e "$(date '+%F %T') - ${FUNCNAME[2]}(${BASH_LINENO[1]}): $msg_type - $*" | tee -a "$LOGFILE" >&2
  echo -e "$(date '+%F %T') - ${FUNCNAME[2]}(${BASH_LINENO[1]}): $msg_type - $*" &>> "$LOGFILE"
}

log_ok() {
  ok_info=$(echo -ne '\e[32m OK \e[0m')
  ga_log "$ok_info" $@
}

log_ko() {
  ko_info=$(echo -ne '\e[31m\e[1m KO \e[0m')
  ga_log "$ko_info" $@
}

log_info() {
  msg_info=$(echo -ne '\e[34mINFO\e[0m')
  ga_log "$msg_info" "$@"
}

log_debug() {
  msg_info=$(echo -ne '\e[36mDBG \e[0m')
  ga_log "$msg_info" "$@"
}

log_kernel() {
  module="$1"
  shift
  echo "[$module] $*" > /dev/kmsg
  log_info [$module] "$@"
}

log_warn() {
  msg_info=$(echo -ne '\e[33mWARN\e[0m')
  ga_log "$msg_info" "$@"
}

test_inc() {
  ga_log '--' salut
  log_ok Ceci est un message OK
  log_ko Ceci est un message KO
  log_info Ceci est un message informatif
  log_kernel "switchres" "Found arcade monitor at blabla"
}

initial_switchres()
{
  # First setup of switchres.conf
  [[ -f "$SWITCHRES_CONF" ]] && return 0
  log_info "Creating switchres.conf config..."
  echo "\
threads=1
ff=1
aspect=4/3" >> "$SWITCHRES_CONF"
  chown "$_USER":"$_GROUP" "$SWITCHRES_CONF"
  return 0
}


# Say something to the user
tell() {
  _speed=120
  log_info "Tell user: $1"
  # Don't show ALSA errors
  espeak-ng -s "$_speed" "$1" 2>/dev/null
}


# Output message and tell it to user
log_and_tell() {
  log_info "$1"
  tell "$1"
}


# Source: https://ouep.eu/shell/fonction-assert/
assert () {
  # First parameter is the message in case the assertion is not verified
  message="$1"

  # The remaining arguments make the command to execute
  shift

  # Run the command, $@ ensures arguments will remain in the same position.
  # "$@" is equivalent to "$1" "$2" "$3" etc.
  "$@"

  # Get the return code
  rc=$?

  # If everything is okay, there's nothing left to do
  [[ $rc == 0 ]] && return 0

  # An error occured, retrieved the line and the name of the script where
  # it happend
  if [[ ${FUNCNAME[1]} == assert_info ]] ; then
    set $(caller 1)
  else
    set $(caller)
  fi

  # Output an error message on the standard error
  # Format: date script [pid]: message (linenumber, return code)
  log_ko "$2 [$$]: $message (line=$1, rc=$rc)"

  # Exit with the return code of the assertion test
  return $rc
}


assert_info() {
  # If the assert failed and the inform function does exist
  assert "$@"
  local rc=$?
  if [[ $rc != 0 ]] && declare -F inform ; then
    inform "$1"
    sleep 2
  fi
  return $rc
}


get_config_value() {
  file="$1"
  key="$2"
  separator="$3"
  local value=

  [[ -z $separator ]] && separator="="

  if [[ ! -f "$file" ]] ; then
    log_ko "$file doesn't exist, aborting"
    return 1
  fi

  if ! value=$(grep -E "^$key$separator" "$file") ; then
    return 2
  fi

  echo "$value" | awk -F "$separator" '{print $2}'
  return $?
}


get_mame_config_value() {
  key="$1"
  local inifile="$MAME_INI"
  local value=

  # Again lousy bash polymorphism
  # Either pass just a key so the func will default to the $MAME_INI file
  # Either pass a file then a key to browse that file
  if [[ $# == 2 ]] ; then
    key="$2"
    inifile="$1"
  fi

  if [[ ! -f "$inifile" ]] ; then
    log_ko "$file doesn't exist, aborting"
    return 1
  fi

  if ! value=$(grep -E "^[[:space:]]*$key[[:space:]]+" "$inifile") ; then
    return 2
  fi

  echo "$value" | grep -oE "[[:punct:][:alnum:]]+$"
  return $?
}


set_config_value() {
  file="$1"
  key="$2"
  value="$3"
  separator="$4"

  [[ -z $separator ]] && separator="="

  # Better crash if the file doesn't exist. Could be a mistype, a patch missing.
  # So let's not suppose, it's up to the coder take care of that
  if [[ ! -f "$file" ]] ; then
    log_ko "$file doesn't exist, aborting"
    return 1
  fi

  if grep -qE "^$key$separator" "$file" ; then
    log_info "Editing key $key$separator$value in $file"
    sed -riE "s+^(^$key$separator).*+\1$value+g" "$file"
    ret=$?
  else
    log_info "Adding key $key$separator$value to $file"
    echo "$key$separator$value" >> "$file"
    ret=$?
  fi

  if [[ $ret -eq 0 ]] ; then
    log_ok "Successfully set $key$separator$value in $file"
  else
    log_ko "Couldn't set $key$separator$value in $file"
  fi
  return $ret
}


set_mame_config_value() {
  # If there are 3 parameters, then 1st one is a specific ini file
  # Bash polymorphism ...
  local inifile="$MAME_INI"

  if [[ $# == 3 ]] ; then
    inifile="$1"
    shift
  fi

  key="$1"
  value="$2"

  # Better crash if the file doesn't exist. Could be a mistype, a patch missing.
  # So let's not suppose, it's up to the caller take care of that
  assert_info "$inifile doesn't exist, aborting" test -f "$inifile" ||return 1

  # Update /etc/switchres.ini
  if grep -E -q "^[[:space:]]*$key[[:space:]]+" "$inifile" ; then
    log_info "Editing key $key=$value in $inifile"
    sed -Ei "s|^([[:space:]]*${key}[[:space:]]+).*|\1${value}|g" "$inifile"
    ret=$?
  else
    echo "$key			  $value" >> "$inifile"
    ret=$?
  fi

  if [[ $ret -eq 0 ]] ; then
    log_ok "Successfully set $key $value in $inifile"
  else
    log_ko "Couldn't set $key $value in $inifile"
  fi
  return $ret
}


set_ra_config_value() {
  set_config_value "$MOTHER_OF_ALL"/configs/retroarch/retroarch.cfg "$1" "$2" " = "
}


get_ra_config_value() {
  local cfg_file="$MOTHER_OF_ALL"/configs/retroarch/retroarch.cfg
  local val=""
  # Again some ugly polymorphism
  if [[ $# == 2 ]] ; then
    cfg_file="$1"
    shift
  fi
  val="$(get_config_value "$cfg_file" "$1" " = " | tr -d '"')"
  # GA_USER doesn't exist here :( this is bad
  local usr="$GA_USER"
  [[ -z $GA_USER ]] && usr=arcade
  if [[ $(id -u) == "0" ]] ; then
    echo "$val" | grep -q '~' && val="$(su "$usr" -c "eval echo $val")"
  else
    echo "$val" | grep -q '~' && val="$(eval "echo $val")"
  fi
  echo $val
}


# Get a value from a ini file
# $1 the source file
# $2 the section
get_ini_value() {
  f="$1"
  section="$2"
  param="$3"

  assert "$f doesn't exist"  test -f "$f" || return 1
  sed -nr "/^\[${section}\]/ { :l /^${param}[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" "$f"
}


# Sets a value in a ini file
# $1 the source file
# $2 the section
# $3 the parameter
# $4 the value to set
set_ini_value() {
  f="$1"
  section="$2"
  param="$3"
  value="$4"

  assert "$f doesn't exist"  test -f "$f"
  # Check the section exists. If not, add it
  grep -E -q "^\[${section}\]$" "$f" || echo -e "\n[${section}]" >> "$f"

  # 2 cases here: the param exists or not
  sed -i "/^\[${section}\]$/,/^\[/ s/^${param}[ ]*=.*/${param}=${value}/" "$f"
  val="$(get_ini_value "$f" "$section" "$param")"
  # If the parameter doesn't exist, the previous sed won't add it, so chck the situation
  [[ "$value" != "$val" ]] && sed -i "/^\[${section}\]$/a ${param}=${value}" "$f"
}


# Returns the path to the syslinux.cfg file
find_syslinux_file() {
  [[ -d /sys/firmware/efi ]] && echo "/boot/EFI/syslinux/syslinux.cfg" || echo "/boot/syslinux/syslinux.cfg"
}


# Remove a jernel parameter
# $1 parameter name (ex: monitor)
kernel_remove_param() {
  param="$1"
  syslinuxfile="$(find_syslinux_file)"
  sed -Ei "/^append root=.*/ s/${param}=[[:alnum:][:punct:]]+//g" "$syslinuxfile"
}


# Set a new value for a kernel parameter
# $1 the parameter name (ex: video)
# $2 the new value
kernel_edit_param() {
  param="$1"
  value="$2"
  syslinuxfile="$(find_syslinux_file)"
  log_info "Setting kernel parameter $param to $value"
  # Check first if the param exists
  if grep "^append root=" "$syslinuxfile" | grep "${param}=" &>/dev/null; then
    # It exists, so edit it
    # The group keeps the trailing spaces, the edit adds some more,
    # so shrink contiguous spaces to just 1
    sed -Ei "s/^(append root=.*)[ ]*${param}=[[:alnum:][:punct:]]*[ ]*(.*)/\1 ${param}=${value} \2/" "$syslinuxfile"
    sed -Ei "/^append root=.*/ s/[[:space:]]{2,}/ /g" "$syslinuxfile"
  else
    # Nope, so add it
    sed -Ei "/^append root=.*/ s/$/ ${param}=${value}/" "$syslinuxfile"
  fi
}


# Return a kernel parameter value
# $1 the parameter (ex: video)
# WARNING: if the parameter exists multiple times, unexpected behaviour
kernel_get_param() {
  param="$1"
  # Could have went with /proc/cmdline this time, but ok. Better use the syslinux.cfg
  # in case of multiple consecutive edits in a session before a reboot
  syslinuxfile="$(find_syslinux_file)"
  grep "^append root=" "$syslinuxfile" | grep -E -o "${param}=[[:alnum:][:punct:]]+" | sed "s/${param}=//"
}

# Same as above, but using cmdline
# $1 the parameter (ex: video)
# WARNING: if the parameter exists multiple times, it should return several lines
kernel_get_cmdline_param() {
  param="$1"
  cmdline=$(</proc/cmdline)
  for p in $cmdline ; do
    case "$p" in
      "$param"=*)
        echo "${p#*=}"
        ;;
    esac
  done
}

# As the video= parameter is full of options, dedicate some functions to it
# $1 is the video= subparameter to remove
kernel_remove_video_subparam() {
  param="$1"
  syslinuxfile="$(find_syslinux_file)"
  # If there are several video= params, we're screwed ...
  videoval="$(kernel_get_param video)"
  new_videoval="$(echo $videoval | sed -E "s/[,]?${param}=[[:alnum:][:punct:]]+//")"
  kernel_edit_param video "$new_videoval"
}


kernel_edit_video_subparam() {
  param="$1"
  value="$2"
  syslinuxfile="$(find_syslinux_file)"
  videoval="$(kernel_get_param video)"
  # Check first if the param exists
  if echo ${videoval} | grep "${param}=" &>/dev/null; then
    # It exists, so edit it
    new_videoval="$(echo ${videoval} | sed -E "s/${param}=[[:alnum:][:punct:]]+/${param}=${value}/")"
  else
    # Nope, so add it
    new_videoval="${videoval},${param}=${value}"
  fi
  kernel_edit_param video "$new_videoval"
}


# Finds if a value into a comma seperated list
# A space is considered part of a value: "val1,val2" is not the same as "val1, val2"
# $1:the list (ex: "val1,val2,val3").
# $2: the value to find
# return: 0 if the value was found, 1 otherwise
list_has_value() {
  list="$1"
  value="$2"
  delimiter=","

  for s in $(echo "$list" | tr "$delimiter" '\n') ; do
    [[ $s == $value ]] && return 0
  done

  return 1
}


# Set a value for a parameter in pacman.conf
# Handles that config parameter can be commented at first
set_pacman_config_value() {
  key="$1"
  value="$2"
  pattern="[[:space:]]*$key[[:space:]]+="

  # First check if the parameter has to be uncommented
  if grep -q -E "^#$pattern" /etc/pacman.conf ; then
    log_info "Uncommenting key $key in /etc/pacman.conf"
    sed -E -i "/[[:space:]]*$key[[:space:]]+=/s/^#//" /etc/pacman.conf
  fi
  set_config_value /etc/pacman.conf "$key" "$value" '[[:space:]]*=[[:space:]]*'
}
