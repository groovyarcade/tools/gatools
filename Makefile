DESTDIR ?= /opt/gatools

.PHONY: install
install:
	install -d $(DESTDIR)
	install -d $(DESTDIR)/include
	install -d $(DESTDIR)/video
	install -d $(DESTDIR)/support
	install -d $(DESTDIR)/efc

	install -m 755 include/* $(DESTDIR)/include
	install -m 755 video/* $(DESTDIR)/video
	install -m 755 support/* $(DESTDIR)/support
	install -m 755 efc/* $(DESTDIR)/efc

.PHONY: uninstall
uninstall:
	rm -rf $(DESTDIR)
