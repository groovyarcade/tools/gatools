#!/bin/bash
readonly __dir__=$(dirname "${BASH_SOURCE[0]}")
source "$__dir__"/../include/includes.sh
source "$__dir__"/monitor.sh
source "$__dir__"/inform.sh

# Switchres pattern we expect in EDID
SWR_PATTERN=Switchres
EDID_SWR_PATTERN="Display Product Serial Number: "
EDID_SWR_MONITOR="Display Product Name: "

#

# Return 0 if $1 is a valid switchres monitor. Otherwise return 1
is_valid_switchres_monitor() {
  if command -v switchres >/dev/null && ! switchres 640 480 60 --monitor "$1" 2>&1 | grep -q -e "Monitor type unknown" -e "Monitor type not specified" ; then
    return 0
  else
    return 1
  fi
}


# Test if an EDID does exist for the given monitor
# $1: a monitor type
# return: 0 if it exists
edid_exists() {
  local edid="$1".bin
  archiso_initramfs=/run/archiso/bootmnt/groovyarcade/boot/x86_64/initramfs-linux-15khz.img
  initramfs_file=/boot/initramfs-linux-15khz.img
  # The initramfs file must be adapted depeding if we're on the iso or not
  [[ -f $archiso_initramfs ]] && initramfs_file="$archiso_initramfs"
  [[ -f /usr/lib/firmware/edid/"$edid" ]] && lsinitcpio -l "$initramfs_file" | grep -q "edid/$edid$"
  return $?
}


# Return EDID kernel command line
# $1: connector name
# $2: monitor type
edid_kernel() {
  local connector="$1"
  local edid="$2"

  # Make sure the .bin exists and is in initramfs
  if ! edid_exists "$edid" ; then
    log_ko "EDID $edid doesn't exist in /usr/lib/firmware/edid or in initramfs"
    return 1
  fi

  # We're good to go now!
  echo "drm.edid_firmware=${connector}:edid/${edid}.bin"
  return 0
}


# Dynamically load a EDID for given monitor
# $1: connector name
# $2: monitor type
load_edid_firmware() {
  local connector="$1"
  local edid="$2"

  # Make sure the .bin exists and is in initramfs
  if ! edid_exists "$edid" ; then
    log_ko "EDID $edid doesn't exist in /usr/lib/firmware/edid or in initramfs"
    return 1
  fi

  log_info "Loading ${edid}.bin for connector $connector"
  # This will likely not work. At least not on analogue connectors. But let's keep it
  echo -n "${connector}:edid/${edid}.bin" > /sys/module/drm/parameters/edid_firmware
  # This one is overkill, but it has the expected result
  # First we need the card number. Luckily ls should return only 1 line
  card_number=$(ls -d /sys/class/drm/card*-"${connector}" | grep -oE "card[0-9]+" | grep -oE "[0-9]+")
  cat /usr/lib/firmware/edid/"$edid".bin > /sys/kernel/debug/dri/"${card_number}"/"$connector"/edid_override
  return $?
}


# Apply some kernel command line / module parameters on the fly
# $1: should be like a kernel parameter
# return: not decided yet
apply_kernel_parameters() {
  local cmdline="$1"
  for p in $cmdline ; do
    case "$p" in
      drm.edid_firmware=*)
        param="${p#*=}"
        conn="${param%:*}"
        edid=$(basename "${param#*:}" .bin)
        load_edid_firmware "$conn" "$edid"
        ;;
    esac
  done
}


# Return the real monitor name for switchres
normalize_model_name () {
  model="$1"

  case "$model" in
    arcade_15_25_)
      model=arcade_15_25_31
      ;;
  esac

  echo "$model"
}


get_edid_value() {
  edid_file="$1"
  value="$2"
  edid-decode "$edid_file" | grep -E "$value" | sed -e "s/$value//" 2>/dev/null
}


# Return if the monitor chosen at boot is 15, 25 or 31kHz. Only useful when
# booting from ISO. Unknown result otherwise
get_monitor_sync_from_kernel_cmdline() {
  # Luckily this returns only 1 result if multiple video= exist
  # The script doesn't check if ALL video= set the same resolution
  grep -oE "video=[A-Z0-9-]+:([0-9]{3}x[0-9]{3}[e]?[czy])" /proc/cmdline | grep -oE "[0-9]{3}x[0-9]{3}[e]?[czy]" | head -1
  return $?
}


get_kernel_parameter_value() {
  uprm="$1"
  val=""
  cmdline=$(</proc/cmdline)
  for p in $cmdline ; do
    case "$p" in
      "$uprm"=*)
        param="${p#*=}"
        val+="${param%:*}"
        ;;
    esac
  done

  if [[ -n $val ]] ; then
    echo $val
    return 0
  else
    return 1
  fi
}


# DEPRECATED: legacy from the past, should be removed if not used anywhere
set_xorg_conf() {
  local monitor="$1"
  log_info "Creating X Windows configuration for $MONITOR monitor..."
  rm -f /etc/X11/xorg.conf
  /usr/local/bin/xorg.sh "$MONITOR" > /etc/X11/xorg.conf
  chown arcade.nobody /etc/X11/xorg.conf
  if [ -f "/etc/X11/xorg.conf" ]; then
    cp -f /etc/X11/xorg.conf "$CONFIG_DIR"/
  fi
}


# That's originally from gasetup, slightly reworked
set_monitor() {
  local MONITOR=$1

  if [[ -z $MONITOR ]] ; then
    log_ko "Monitor is empty!"
    return 1
  fi

  log_info "Setup $MONITOR monitor accross the system..."

  log_info "Setup $MAME_INI for $MONITOR monitor..."
  set_mame_config_value monitor "$MONITOR"
  chown arcade:nobody "$MAME_INI"

  log_info "Setup ga.conf for $MONITOR monitor..."
  set_config_value "$GA_CONF" monitor "$MONITOR"

  log_info "Setup $SWITCHRES_INI for $MONITOR monitor..."
  set_mame_config_value "$SWITCHRES_INI" monitor "$MONITOR"

  [[ "$MONITOR" = "lcd" ]] && return 0

  # VESA monitors sepcific
  if [[ ! "$MONITOR" =~ ^vesa ]]; then
    log_info "Disabling GLSL CRT emulation for LCD monitor"
    set_mame_config_value gl_glsl 0
    set_mame_config_value aspect "4:3"
  fi
  return 0
}


ask_user_confirmation() {
  local cardnum="$1"
  local ttynum=8
  local fbnum=
  local result_file=/tmp/screen_confirm_result
  local ret=

  log_debug "Was called with cardnum=$cardnum"
  # Find the fb associated to the GFX card
  fbnum=$(ls -d /sys/class/drm/card"$cardnum"/device/graphics/fb* | grep -oE "[0-9]+$")

  # Map the tty to the fb
  #(($fbnum > 0)) && con2fbmap "$ttynum" "$fbnum"
  con2fbmap "$ttynum" "$fbnum"

  # Pop the dialog
  sudo openvt -c "$ttynum" -f -s -w -- bash "$__dir__"/screen_confirm.sh "$result_file"

  # Restore the tty to the fb
  #(($fbnum > 0)) && con2fbmap "$ttynum" 0

  ret=$(< "$result_file")
  return $ret
}


# cycle the output state of a vidoe card, warn the user.
# $1 must look like /sys/class/drm/card0-DP-1
# return:
#  0: there is an unknown monitor
#  1: there is no monitor
#  2: there is a monitor that must be forced to enable
#  3: found an EDID, but not a switchres one
#  4: found a switchres EDID
test_connector() {
  local connector="$1"
  local conn="${connector#*/card*-}"
  local card_num=
  card_num="$(basename "${connector%%-*}")"
  local found_conn=
  local forced=

  log_and_tell "Testing connector $conn on $card_num"
  log_info "Current connector enabled/status: $(< "$connector"/enabled)/$(< "$connector"/status)"
  log_and_tell "  Turning it on"
  echo "detect" > "$connector"/status
  status=$(< "$connector"/status)
  edid_size=$(wc -c < "$connector"/edid)
  log_info "${connector#*card*-} EDID is $edid_size bytes"

  # Testing on .../status == connected rather than .../enabled == enabled
  # Had some weird behaviour with nouveau where a screen was working but not
  # considered as enabled
  # Do we have an EDID ?
  # WARNING : EDID is available only if status was set to detect and not on
  if [[ $edid_size -gt 0 ]] && [[ $(< "$connector"/status) == "connected" ]] ; then
    # Is it a switchres EDID ?
    if get_edid_value "$connector"/edid "^[[:space:]]*$EDID_SWR_PATTERN" | grep -q "$SWR_PATTERN"; then
      # Hurray! a switchres EDID!
      log_and_tell "Found a valid switchres E D I D on connector $conn"
      # 2 possible cases :
      #   - it's a sweet EDID dongle, so rc=4
      #   - EDID forced at boot, so we may be with a DP, HDMI etc... that
      #     must have an EDID to have only 1 SR resolution
      if get_kernel_parameter_value drm.edid_firmware > /dev/null ; then
        log_info "The EDID was forced on kernel command line"
        rc=5
      else
        log_info "Skipping connector $conn as it is already enabled"
        rc=4
      fi
    else
      # The EDID is not a switchres one, so probably a LCD panel
      log_and_tell "Found a LCD panel or modern CRT on connector $conn"
      rc=3
    fi
    log_and_tell "  Turning it off and wait for 2 seconds" &
    echo "off" > "$connector"/status
    sleep 2
    wait
    return $rc
  fi

  # If nothing was found on a digital connector, then just quit
  if [[ $status != "connected" ]] && \
   ! [[ $conn =~ VGA- ]] && \
   ! [[ $conn =~ DVI-I- ]] ; then
    log_and_tell "  No screen found. Turning it off and wait for 2 seconds" &
    echo "off" > "$connector"/status
    sleep 2
    wait
    return 1
  fi

  # Only force enable analog connectors. Digital should have EDID connected.
  if [[ $status != "connected" ]] ; then
    if [[ $conn =~ VGA- ]] || [[ $conn =~ DVI-I- ]] ; then
      forced=1
      log_and_tell "No monitor detected, forcing analog connector to on"
      echo "on" > "$connector"/status
    fi
  fi
  #~ if dialog --stdout --title "Testing connector $card_num/$conn" --timeout 5 --msgbox "Press ENTER if you see this test." 5 50 ; then
    #~ log_info "$conn was validated by user"
    #~ found_conn=1
  #~ fi
  if ask_user_confirmation "${card_num#card}" ; then
    log_info "$conn was validated by user"
    found_conn=1
  fi
  log_and_tell "  Turning it off and wait for 2 seconds" &
  echo "off" > "$connector"/status
  sleep 2
  wait

  # Return code depends on connector behaviour
  if [[ ! -z $found_conn ]] ; then
    # If the connector was found but had to be forced to on, return 2.
    # Otherwise, return 0
    if [[ ! -z $forced ]] ; then
      return 2
    else
      return 0
    fi
  fi
  # No screen was found
  return 1
}


# Force status to all connectors
# $1: new status (on/off/detect/on-digital)
switch_all_connectors() {
  status="$1"
  # If we turn on, let's only focus on analogue capable outputs, digital ones
  # like HDMI or DP work without such hacks
  if [[ $status == on ]] ; then
    for card in /sys/class/drm/card*-{DVI-I,VGA}-*; do
      log_info "$card will turn to $status"
      echo "$status" > "$card"/status
    done
  else
    for card in /sys/class/drm/card*-*; do
      log_info "$card will turn to $status"
      echo "$status" > "$card"/status
    done
  fi
}



# Return if the specified card supports low pixel clocks
# $1: a card number like card0
card_supports_low_dot_clock() {
  local card="$1"
  local pciid=
  local module=
  local modules_ok_for_low_dotclock="radeon amdgpu"

  # Get the card ID
  pciid="$(readlink -f /sys/class/drm/"$card" | grep -Eo "[0-9a-z]{2}:[0-9a-z]{2}\.[0-9a-z]" | tail -1)"
  module=$(lspci -ks "$pciid" | grep "Kernel driver in use:" | grep -Eo "[[:alnum:]]+$")
  [[ $modules_ok_for_low_dotclock =~ $module ]] && return 0 || return 1
}

# Return if the specified connector supports low pixel clocks
# $1: a connector like DVI-I-1
connector_supports_low_dot_clock() {
  local conn="$1"
  local cardnum=

  cardnum=$(find /sys/class/drm -name "*-$conn" | grep -oE "card[0-9]+")
  [[ -z $cardnum ]] && return 1
  card_supports_low_dot_clock "$cardnum" && return 0 || return 1
}


# Browse through all conectors, echo result to STDOUT
test_all_connectors() {
  local return_string=
  local connectors_to_be_forced
  local pciid=
  local modules_ok_for_edid="radeon amdgpu"
  local is_apu=

  # Let's first turn everything on, show a message to make sure the user sees it
  # Otherwise, we're screwed
  switch_all_connectors on
  #if ! dialog --stdout --title "Lights on, test1!" --timeout 5 --msgbox "Press ENTER if you see this test." 5 50 ; then
  #  log_ko "User did nott see the first message, abort"
  #  return 1
  #fi
  dialog --stdout --title "Lights on, test 1!" --timeout 5 --msgbox "Press ENTER if you see this test." 5 50
  dialog --stdout --title "About to begin" --timeout 5 --msgbox "We'll now test all cards outputs one by one to determine your setup. Meanwhile the computer will speak to you and describe its actions. The first step is to turn everything off, then testing will begin." 9 50

  switch_all_connectors off
  for card in /dev/dri/by-path/*-card ; do
    edid_ok=
    # Transform /dev/dri/by-path/pci-0000:00:02.0-card in 00:02.0
    # Warning, can be in the shape of /dev/dri/by-path/pci-0000:0c:00.0-card -> alphanumeric, not just numbers
    pciid=$(basename "$card" | grep -Eo "[[:alnum:]]{2}:[[:alnum:]]{2}\.[[:alnum:]]")
    card_num=$(basename "$(readlink -f "$card")")
    # Get module name from a line like "Kernel driver in use: nvidia"
    module=$(lspci -ks "$pciid" | grep "Kernel driver in use:" | grep -Eo "[[:alnum:]]+$")
    lspci -vs "$pciid" | grep -q "Onboard IGD" && is_apu=1 && log_info "Device $pciid is an APU"
    # Check if EDID works fine with this driver
    [[ $modules_ok_for_edid =~ $module ]] && edid_ok=s
    log_debug "$pciid $card_num -> $module / Use EDID: $edid_ok"

    # Cycle through card outputs
    for connector in /sys/class/drm/"$card_num"-* ; do
      # First test case : connector is ON and we found an EDID
      # can't use -s test as edid size is always 0
      # We'll poweron the connector and ask user confirmation
      test_connector "$connector"
      rc=$?
      case $rc in
        5)  log_ok "${connector#*card*-} has a forced Switchres EDID"
            return_string=$(echo -e "${return_string}\n${connector#*card*-}: ${edid_ok}do")
            ;;
        4)  log_ok "${connector#*card*-} has a Switchres EDID"
            return_string=$(echo -e "${return_string}\n${connector#*card*-}: ${edid_ok}o")
            ;;
        3)  log_ok "${connector#*card*-} seems to be a LCD"
            return_string=$(echo -e "${return_string}\n${connector#*card*-}: ${edid_ok}l")
            ;;
        2)  log_ok "${connector#*card*-} has a monitor but needs to be forced"
            return_string=$(echo -e "${return_string}\n${connector#*card*-}: ${edid_ok}e")
            connectors_to_be_forced="$connector $connectors_to_be_forced"
            ;;
        1)  log_info "${connector#*card*-} has no screen"
            return_string=$(echo -e "${return_string}\n${connector#*card*-}: ${edid_ok}x")
            ;;
        0)  log_ok "${connector#*card*-} has an unknown arcade monitor"
            return_string=$(echo -e "${return_string}\n${connector#*card*-}: ${edid_ok}r")
            ;;
      esac
      [[ -n $is_apu ]] && return_string+="a"
    done # loop on card's connectors
  done # loop on cards

  switch_all_connectors detect
  # Turn on screens that need to be forced
  for s in $connectors_to_be_forced ; do
    log_info "Turning $s to ON as required"
    echo on > "$s/status"
  done

  echo "$return_string" | grep -v "^$" | sort
  return 0
}

# Select the best possible connector
# $1 is a list of connectors from test_all_connectors
# return: 0 if a connector was selected (get it in STDOUT), otherwise 1
choose_connector() {
  local connectors="$1"

  # CASE 1: there is a switchres EDID dongle and driver supports 15kHz EDID
  # We don't keep a connector which card wouldn't accept the EDID
  # Technically speaking, o only is impossible and not usable anyway
  if echo "$connectors" | grep -qE ": s[d]?o$" ; then
    echo "$connectors" | grep -E ": s[d]?o$" | head -1
    return 0
  fi

  # CASE 2: found EDID-less monitor
  if echo "$connectors" | grep -qE ": [s]?r$" ; then
    echo "$connectors" | grep -E ": [s]?r$" | head -1
    return 0
  fi

  # CASE 3: found EDID-less monitor that needs to be forced enabled
  if echo "$connectors" | grep -qE ": [s]?e$" ; then
    echo "$connectors" | grep -E ": [s]?e$" | head -1
    return 0
  fi

  # CASE 4: that's a LCD panel, or at least we can trust its EDID
  # Still, if it's a CRT monitor with EDID, we're misconfiguring then
  if echo "$connectors" | grep -qE ": [s]?l$" ; then
    echo "$connectors" | grep -E ": [s]?l$" | head -1
    return 0
  fi

  # Looks like all of the possible cases failed
  echo ""
  return 1
}


# Configure the OS depending on the chosen connector
# $1: a connector in the form "DP-1: os"
# return: the kernel command line parameters to be added
configure_from_connector() {
  local conn="${1%:*}"
  local conn_params="${1#*: }"
  local monitor=
  local monitor_param=
  local enabled=
  local video=
  local edid=
  local knl_res=
  local freq=60
  local prog_or_int=i

  # Reminder on letters signfication:
  #   - r: a monitor seems connected, but no clue on the monitor
  #   - e: connector must be forced enabled as the monitor is not detected by the GFX card
  #   - l: connector has a LCD panel (or at least a non switchres EDID behind)
  #   - s: the driver supports low dotclocks
  #   - o: found a valid switchres EDID on connector
  #   - d: the EDID was forced on kernel command line
  #   - a: is an APU, needs interlace_force_even = 1


  # Display the monitor menu so the user chooses its monitor
  # This is not useful if we found a Switchres EDID
  # [s]e, [s]r, l = !([s]o)
  if [[ ! $conn_params =~ ^[s]?o$ || $conn_params =~ ^[s]?do$ ]] ; then
    # User MUST choose a monitor
    while ! monitor=$(menu_select_monitor "$conn") ; do true ; done
  else
    # Read monitor from EDID
    if f=$(find /sys/class/drm -name "*-$conn" | head -1) ; then
      pattern="^$EDID_SWR_MONITOR"
      monitor=$(get_edid_value "$f"/edid "$pattern")
      monitor=$(normalize_model_name "$monitor")
      # Make sure the monitor exists
      if [[ $monitor != "lcd" ]] && ! monitor_to_kernel_resolution "$monitor" ; then
        log_ko " Monitor '$monitor' is unknown, can't configure GA"
        return 1
      fi
    else
      log_ko "$conn: couldn't read EDID"
      return 1
    fi
  fi

  monitor_param="monitor=$monitor"
  set_monitor "$monitor"
  if [[ $monitor == "lcd" ]] ; then
    echo "$monitor_param"
    return 0
  fi

  # Check whether we should or not enable the connector
  if [[ $conn_params =~ ^[s]?e$ ]] ; then
    enabled=e
  fi

  # we don't care about l or o status, it's up to the edid
  # Just handle non EDID monitors
  if [[ $conn_params =~ ^[er]$ ]] ; then
    # Must use super resolution
    if ! knl_res=$(monitor_to_kernel_resolution "$monitor" "$freq" "$prog_or_int" 1) ; then
      log_ko "Couldn't determine monitor resolution"
      return 1
    fi
    # Tell mame about the min_dotclock
    set_mame_config_value dotclock_min "25.0"
    set_mame_config_value "$SWITCHRES_INI" dotclock_min "25.0"
  elif [[ $conn_params =~ ^s[er]$ ]] ; then
    # Normal resolution, monitor is detected
    if ! knl_res=$(monitor_to_kernel_resolution "$monitor" "$freq" "$prog_or_int") ; then
      log_ko "Couldn't determine monitor resolution"
      return 1
    fi
  fi

  # The connector is from an APU
  if [[ $conn_params =~ a ]] ; then
    set_mame_config_value "$SWITCHRES_INI" interlace_force_even "1"
  fi

  # Some cases (the d switch) require we use EDID instead of a SR resolution
  # But some APUs/connectors will be forced as EDID at boot, let's respect that
  if [[ $monitor != "lcd" ]] ; then
    # Need to use an EDID
    if [[ $conn_params =~ d ]] ; then
      #video="$(edid_kernel ${conn} "$monitor")"
      # Get the same EDID as boot
      video="drm.edid_firmware=${conn}:$(cat /sys/module/drm/parameters/edid_firmware)"
    elif grep -q "drm.edid_firmware" /proc/cmdline ; then
      video="video=${conn}:e drm.edid_firmware=${conn}:edid/${monitor}.bin"
    else
      video="video=${conn}:${knl_res}${enabled}"
    fi
  fi

  [[ $conn_params =~ ^[er]$ ]] && set_mame_config_value aspect "4:3"
  echo "$monitor_param" "$video"
}


# Let user validate the configuration that was decided
# $1: the connector with arcade monitor
# return: 0 if user agreed, 1 if refused
inform_user() {
  local conn="${1%:*}"
  local conn_params="${1#*: }"
  local user_message=

  user_message="Here are the results of auto-detection:"
  if [[ -z $conn ]] ; then
    user_message="$(echo "${user_message}\n  - No connector was selected. ")"
  else
    user_message="$(echo "${user_message}\n  - Connector $conn was selected. ")"
  fi
  echo "$conn_params" | grep -q 'o' | grep -v 'd' && user_message="$(echo "${user_message}\n  - Found a valid switchres EDID, no other configuration will be needed.")"
  echo "$conn_params" | grep -q 'r' && user_message="$(echo "${user_message}\n  - The arcade monitor was detected, but it didn't return any info.")"
  echo "$conn_params" | grep -q 's' && user_message="$(echo "${user_message}\n  - Your video card supports low dotclocks, you will enjoy the best GroovyMAME experience")"
  ! echo "$conn_params" | grep -q 's' && user_message="$(echo "${user_message}\n  - Your video card doesn't support low dotclocks, super resolutions will be used instead.")"
  echo "$conn_params" | grep -q 'e' && user_message="$(echo "${user_message}\n  - The arcade monitor couldn't be natively detected, the output will be forced at boot.")"
  echo "$conn_params" | grep -q 'l' && user_message="$(echo "${user_message}\n  - Found a monitor that could be a LCD panel.")"
  echo "$conn_params" | grep -q 'a' && user_message="$(echo "${user_message}\n  - Your video card is an APU and needs interlace_force_even=1.")"

  user_message="$(echo "${user_message}\n\nDo you want to validate these new settings?")"
  dialog --backtitle "Detection configuration" \
    --title "Testing results" \
    --yesno "$user_message" 25 60
  return $?
}

auto_configure() {
  local available_connectors
  local chosen_connector
  local user_connector

  # Test all connectors, interact with user
  available_connectors="$(test_all_connectors)"
  log_debug "Available connectors: $available_connectors"

  # Now we know connectors configuration, determine the best one
  chosen_connector="$(choose_connector "$available_connectors")"
  log_debug "software-chosen connector: ${chosen_connector%:*}"

  # Show the menu to select a connector if more than 2 monitors are connected
  user_connector="$chosen_connector"
  if (( $(echo "$available_connectors"| grep -vE "x$" | wc -l) > 1 )) ; then
    user_connector=$(menu_connector "${chosen_connector%:*}" "$available_connectors")
    user_connector=$(echo "$available_connectors" | grep "$user_connector")
  fi
  log_debug "user-chosen connector: ${user_connector%:*}"

  ! inform_user "$user_connector" && return 1
  echo "$available_connectors"

  # Setup GA according to the selected connector
  kernel_params="$(configure_from_connector "${user_connector}")"
  log_debug "Kernel parameters: "
  log_debug "$kernel_params"
  set_config_value "$GA_CONF" kernel_video_cmdline "$kernel_params"
  set_config_value "$GA_CONF" connector "${user_connector%:*}"

  # Apply possible kernel parameters
  apply_kernel_parameters "$kernel_params"

  return 0
}
