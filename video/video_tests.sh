readonly __dir__=$(dirname "$0")
source "$__dir__"/video.sh

rm groovy.log 2>/dev/null
#~ find_switchres_edid ; echo "EDID was found: $?"
#~ test_connector /sys/class/drm/card0-HDMI-A-1
#~ test_connector /sys/class/drm/card0-HDMI-A-1 off
apply_kernel_parameters "monitor=lcd drm.edid_firmware=DP-1:edid/generic_15.bin" ; exit
test_all_connectors
#auto_configure
exit

#configure_from_connector "HDMI-A-1: so" ; exit

# Unit testing for configure_from_connector()
for t in r e o l sr se so ; do
  r="VGA-1: $t"
  echo "TESTING: $r"
  configure_from_connector "VGA-1: $t"
  inform_user "VGA-1: $t"
done
exit

#~ results="DP-1: l
#~ DP-2: e
#~ DP-3: sr
#~ DVI-I-1: l
#~ HDMI-A-1: o
#~ HDMI-A-2: x
#~ HDMI-A-3: x"
#~ echo -e "Choosing among\n$results"
#~ echo -n "  --> RESULT: "
#~ conn=$(choose_connector "$results")
#~ echo $conn
#~ configure_from_connector "$conn"

#~ results="DP-1: l
#~ DP-2: e
#~ DP-3: sr
#~ DVI-I-1: l
#~ HDMI-A-1: so
#~ HDMI-A-2: x
#~ HDMI-A-3: x"
#~ echo -e "Choosing among\n$results"
#~ echo -n "  --> RESULT: "
#~ conn=$(choose_connector "$results")
#~ echo $conn
#~ configure_from_connector "$conn"

#~ results="DP-1: x
#~ DP-2: e
#~ DP-3: x
#~ DVI-I-1: l
#~ HDMI-A-1: x
#~ HDMI-A-2: x
#~ HDMI-A-3: x"
#~ echo -e "Choosing among\n$results"
#~ echo -n "  --> RESULT: "
#~ conn=$(choose_connector "$results")
#~ echo $conn
#~ configure_from_connector "$conn"

#~ results="DP-1: x
#~ DP-2: x
#~ DP-3: x
#~ DVI-I-1: l
#~ HDMI-A-1: x
#~ HDMI-A-2: x
#~ HDMI-A-3: x"
#~ echo -e "Choosing among\n$results"
#~ echo -n "  --> RESULT: "
#~ conn=$(choose_connector "$results")
#~ echo $conn
#~ configure_from_connector "$conn"

results="HDMI-A-1: sx
DVI-I-1: sl
VGA-1: so"
echo -e "Choosing among\n$results"
echo -n "  --> RESULT: "
conn=$(choose_connector "$results")
echo $conn
configure_from_connector "$conn"

#~ set_monitor "arcade_15"
#~ set_monitor "lcd"
