#!/bin/bash

get_connector_card() {
  local c="$1"
  local cpath=

  cpath=$(find /sys/class/drm -maxdepth 1 -name "card*-$c" | head -1)
  cpath=$(basename "$cpath")
  echo ${cpath%%-*}
}


get_card_name() {
  local p=/sys/class/drm/"$1"

  id=$(basename `readlink -f $p/device`)
  # now match with lspci
  name=$(lspci -mms $id | cut -d '"' -f4,6 --output-delimiter=" ")
  echo "$name"
}


short_gpu_name() {
  local gpu="$1"
  
  # AMD/ATI GPU
  if [[ "$gpu" =~ ^"Advanced Micro Devices" ]] ; then
    # gpu is like: Advanced Micro Devices, Inc. [AMD/ATI] Cedar [Radeon HD 5000/6000/7350/8350 Series]
    echo "$gpu" | cut -d "[" -f 3 | tr -d ']'
  elif [[ $gpu =~ ^NVIDIA ]] ; then
    # gpu is like: NVIDIA Corporation GK104 [GeForce GTX 760]
    echo "$gpu" | grep -oE "\[.*\]" | tr -d '['  | tr -d ']'
  elif [[ $gpu =~ ^Intel ]] ; then
    # gpu is like: Intel Corporation HD Graphics 530
    echo "$gpu" | sed "s/Intel Corporation //"
  elif [[ $gpu =~ ^InnoTek ]] ; then
    # gpu is like: InnoTek Systemberatung GmbH VirtualBox Graphics Adapter
    echo "VirtualBox GFX"
  else
    echo $gpu
  fi
  return 0
}


gfx_cards_results() {
  local conns="$1"

  for c in $conns ; do
    # Get connector card number
    echo
  done
}


describe_connector_details() {
  local d="$1"
  local str=

  [[ $d =~ [s]?x$ ]] && echo "no monitor found"
  [[ $d =~ [s]?l$ ]] && echo "lcd monitor or modern CRT"
  [[ $d =~ [s]?o$ ]] && echo "LCD or CRT with a switchres EDID"
  [[ $d =~ [s]?r$ ]] && echo "TV or arcade monitor"
  [[ $d =~ [s]?e$ ]] && echo "TV or arcade monitor (forced)"
}


menu_connector() {
  local default="$1"
  shift
  local c=
  local connector=
  local connector_details=
  local card_name=
  local connector_desc=
  local label=
  local menu=()

  IFS=$'\n'
  for c in $1 ; do
    # Skip connectors without output
    echo "$c" | grep -qE "x$" && continue
    connector=${c%:*}
    connector_details=${c#*: }
    card_name=$(get_card_name $(get_connector_card "$connector"))
    connector_desc=$(describe_connector_details "$connector_details")
    label="on $(short_gpu_name "$card_name"): $connector_desc"
    echo $label
    if [[ $default == "$connector" ]] ; then
      menu+=("$connector" "$label")
    else
      menu+=("$connector" "$label")
    fi
    #[[ -z $menu ]] && menu=("$label") || menu+=("$label")
  done
  IFS=
  #~ source easybashgui
  #local title="Please select a connector"
  #supertitle="$title" tagged_menu "${menu[@]}"
  #supertitle="$title" menu "${menu[@]}"
  
  dialog --title "Select connector" \
    --default-item "$default" \
    --stdout \
    --menu "Best connector already selected. Don't change unless you know" 0 0 15 "${menu[@]}"
}


tests() {
  card=$(get_connector_card "HDMI-A-1")
  get_card_name "$card"

  results="DP-1: l
DP-2: e
DP-3: sr
DVI-I-1: l
HDMI-A-1: o
HDMI-A-2: x
HDMI-A-3: x"
  #connectors_results "$results"
  menu_connector "DVI-I-1" "$results"
}
#tests
