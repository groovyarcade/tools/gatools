#!/bin/bash

menu_select_monitor() {
  source easybashgui
  local title="Please select a monitor"
  [[ ! -z "$title" ]] && title="$title for $1"
  supertitle="$title" tagged_menu \
    "generic_15" "Generic 15.7 kHz" \
    "h9110" "Hantarex MTC 9110" \
    "polo" "Hantarex Polo" \
    "pstar" "Hantarex Polostar 25" \
    "m2929" "Makvision 2929D" \
    "ntsc" "NTSC TV - 60 Hz/525 15.734KHz (60 Hz only)" \
    "pal" "PAL TV - 50 Hz/625  15.625KHz (50 Hz only)" \
    "d9200" "Wells Gardner D9200" \
    "d9400" "Wells Gardner D9400" \
    "d9800" "Wells Gardner D9800" \
    "k7000" "Wells Gardner K7000" \
    "k7131" "Wells Gardner 25K7131" \
    "m3129" "Wei-Ya M3129" \
    "ms2930" "Nanao MS-2930, MS-2931" \
    "ms929" "Nanao MS9-29" \
    "arcade_15" "Arcade 15.7 kHz - standard resolution" \
    "arcade_15ex" "Arcade 15.7-16.5 kHz - extended resolution" \
    "arcade_25" "Arcade 25.0 kHz - medium resolution" \
    "arcade_31" "Arcade 31.5 kHz - high resolution" \
    "arcade_15_25" "Arcade 15.7/25.0 kHz - dual-sync" \
    "arcade_15_25_31" "Arcade 15.7/25.0/31.5 kHz - tri-sync" \
    "r666b" "Rodotron 666B-29" \
    "pc_31_120" "Pc Crt 31kHz/120Hz" \
    "pc_70_120" "Pc Crt 70kHz/120Hz" \
    "vesa_480" "VESA GTF  640 x 480" \
    "vesa_600" "VESA GTF  800 x 600" \
    "vesa_768" "VESA GTF 1024 x 768" \
    "vesa_1024" "VESA GTF 1280 x 1024" \
    "lcd" "LCD" 2>/dev/null

  result=$(0<"${dir_tmp}/${file_tmp}")
  echo $result
  [[ -z $result ]] && return 1 || return 0
}


# $1 is the refresh rate
# $2 is i or p for progressive/interlace. Defaults to p
monitor_15k_resolution() {
  local vfreq=${1:-60}
  local frame_type=${2:-p}
  
  if [[ "$vfreq" == "60" ]] ; then
    if [[ "$frame_type" == "p" ]] ; then
      echo "320x240S"
      return 0
    elif [[ "$frame_type" == "i" ]] ; then
      echo "640x480iS"
      return 0
    else
      return 1
    fi
  elif [[ "$vfreq" == "50" ]] ; then
    if [[ "$frame_type" == "p" ]] ; then
      echo "384x288S"
      return 0
    elif [[ "$frame_type" == "i" ]] ; then
      echo "768x576iS"
      return 0
    else 
      return 1
    fi
  else
    return 1
  fi
}

# $1 is the refresh rate. Defaults to 60
# $2 is i or p for progressive/interlace. Defaults to p
monitor_25k_resolution() {
  local vfreq=${1:-60}
  local frame_type=${2:-p}

  if [[ "$vfreq" == "60" ]] ; then
    if [[ "$frame_type" == "p" ]] ; then
      echo "512x384S"
      return 0
    elif [[ "$frame_type" == "i" ]] ; then
      echo "800x600iS"
      return 0
    else
      return 1
    fi
  # 50Hz not yet determined for 25k
  #~ elif [[ "$vfreq" == "50" ]] ; then
    #~ if [[ "$frame_type" == "p" ]] ; then
      #~ echo "384x288S"
      #~ return 0
    #~ elif [[ "$frame_type" == "i" ]]
      #~ echo "768x576iS"
      #~ return 0
    #~ else 
      #~ return 1
    #~ fi
  else
    return 1
  fi
}


monitor_15k_super_resolution() {
  echo "1280x480iS"
}


monitor_25k_super_resolution() {
  echo "800x600iS"
}

# $1 is a valid known monitor
# $2 is the refresh rate. Defaults to 60, unless monitor=pal
# $3 is i or p for progressive/interlace. Defaults to p
# return : STDIN: suggested resolution in XxY:? format (? can be c or z for 15/25kHz)
# and return code = 0 if a valid monitor is specified
# return = 1 when LCD, so trust EDID
# return = 2 if the monitor is unknown
monitor_to_kernel_resolution() {
  local monitor="$1"
  local vfreq="${2:-60}"
  local frame_type="${3:-p}"
  local super_res="$4"

  if [[ $monitor == generic_15 ]] \
    || [[ $monitor ==  arcade_15 ]] \
    || [[ $monitor ==  arcade_15ex ]] \
    || [[ $monitor ==  k7000 ]] \
    || [[ $monitor ==  k7131 ]] \
    || [[ $monitor ==  h9110 ]] \
    || [[ $monitor ==  polo ]] ; then
    if [[ -z "$super_res" ]] ; then
      monitor_15k_resolution "$vfreq" "$frame_type"
    else
      monitor_15k_super_resolution "$vfreq" "$frame_type"
    fi
    return $?
  elif [[ $monitor == pal ]] ; then
    monitor_15k_resolution 50 "$frame_type"
    return $?
  elif [[ $monitor == ntsc ]] ; then
   echo "720x480iS"
   return 0
  elif [[ $monitor == arcade_25 ]] \
    || [[ $monitor == arcade_15_25 ]] \
    || [[ $monitor == ms929 ]] ; then
    if [[ -z "$super_res" ]] ; then
      monitor_25k_resolution "$vfreq" "$frame_type"
    else
      monitor_25k_super_resolution "$vfreq" "$frame_type"
    fi
    return $?
  elif [[ $monitor == arcade_31 ]] \
    || [[ $monitor ==  arcade_15_31 ]] \
    || [[ $monitor ==  arcade_15_25_31 ]] \
    || [[ $monitor ==  m2929 ]] \
    || [[ $monitor ==  d9200 ]] \
    || [[ $monitor ==  d9400 ]] \
    || [[ $monitor ==  d9800 ]] \
    || [[ $monitor ==  m3129 ]] \
    || [[ $monitor ==  pstar ]] \
    || [[ $monitor ==  ms2930 ]] \
    || [[ $monitor ==  r666b ]] \
    || [[ $monitor ==  pc_31_120 ]] \
    || [[ $monitor ==  vesa_480 ]] \
    ; then
    echo "640x480S"
    return 0
  elif [[ $monitor == vesa_600 ]] ; then
    echo "800x600"
    return 0
  elif [[ $monitor == pc_70_120 ]] \
    || [[ $monitor ==  vesa_768 ]] \
    ; then
    echo "1024x768"
    return 0
  elif [[ $monitor == vesa_1024 ]] ; then
    echo "1280x1024"
    return 0
  elif [[ $monitor == lcd ]] ; then
    echo ""
    return 1
  else
    return 2
  fi
}


monitor_type() {
  local monitor="$1"
  if [[ $monitor == generic_15 ]] \
    || [[ $monitor ==  arcade_15 ]] \
    || [[ $monitor ==  arcade_15ex ]] \
    || [[ $monitor ==  k7000 ]] \
    || [[ $monitor ==  k7131 ]] \
    || [[ $monitor ==  h9110 ]] \
    || [[ $monitor ==  polo ]] ; then
    echo "15k"
    return 0
  elif [[ $monitor == arcade_25 ]] \
    || [[ $monitor == arcade_15_25 ]] \
    || [[ $monitor == ms929 ]] ; then
    echo "25k"
    return 0
  else
    return 1
  fi
}
