#!/bin/bash

# This has to be run in a separate tty by openvt. Please mind
#  - openvt doesn't return the exit code of the command run, hence the $dest_file
#  - openvt can't run several commands at once, hence this shell

dest_file="$1"

# Would have loved that dialog --pause wprks, but no. If you remove the OK button,
# Cancel is like OK + dialog exit code is 0 after timeout
#~ dialog --stdout --title "Testing connector $card_num/$conn" \
  #~ --nook \
  #~ --cancel-label toto \
  #~ --pause "Press ENTER if you see this test." 8 50 5
dialog --title "Testing connector $card_num/$conn" \
  --timeout 5 \
  --msgbox "Press ENTER if you see this test." 8 50

echo $? > "$dest_file"

clear
