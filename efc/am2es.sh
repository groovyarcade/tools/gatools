#!/bin/bash

GA_USER=arcade
GA_GROUP=arcade
GA_HOME=/home/arcade
GA_FRONTENDS="$GA_HOME"/shared/frontends

es_output="gamelist.xml"
tmp_gamelist="/tmp/gamelist.xml"
am_gamelist="$GA_FRONTENDS"/attract/romlists/"$1".txt
#am_gamelist="Nintendo - Nintendo Entertainment System".txt


read_attract_value() {
  local file="$1"
  local param="$2"

  grep -E "^$param" "$file" | sed "s/^$param[[:space:]]*//" |sed "s+\$HOME+/home/arcade+"
}


extract_emulator_data()
{
  local em_cfg="$GA_FRONTENDS"/attract/emulators/"$(basename "$am_gamelist" .txt)".cfg
  #local em_cfg="$(basename "$am_gamelist" .txt)".cfg
  _rompath_=$(read_attract_value "$em_cfg" rompath)
  _extensions_=$(read_attract_value "$em_cfg" romext)
  _system_=$(read_attract_value "$em_cfg" system)
}


get_game_description() {
  local game="$1"
  local desc_file="$GA_FRONTENDS/attract/scraper/$(basename "$am_gamelist" .txt)/overview/${game}.txt"
  if [[ -e $desc_file ]] ; then
    cat "$desc_file"
  fi
}


find_real_rom() {
  local rom=$1

  IFS=';'
  for ext in $_extensions_ ; do
    if [[ -e "$_rompath_"/"$rom"$ext ]] ; then
      unset IFS
      echo ./"$rom"$ext
      return 0
    fi
  done
  unset IFS
  return 1
}


get_scraped_data() {
  local rom="$2"
  local type="$1"

  local media_path="$GA_HOME/shared/media/$_system_/$type"
  local exts="png"
  [[ $type == video ]] && exts="mp4"

  # People may have scraped using the AM internal scraper, so take it in consideration
  for p in "$media_path" "$GA_FRONTENDS/attract/scraper/$(basename "$am_gamelist" .txt)" ; do
    for e in $exts ; do
      scraped="$p/$rom".$e
      if [[ -e "$scraped" ]] ; then
        echo "$scraped"
        return 0
      fi
    done
  done
  return 1
}


xml_recode() {
  echo "$1" | sed \
    -e 's/\&/\&amp;/g' \
    -e 's/</\&lt;/g' \
    -e 's/>/\&gt;/g' \
    -e "s/'/\&apos;/g" \
    -e 's/"/\&quot;/g'
}


if [[ ! -f "$am_gamelist" ]] ; then
  echo "ERROR: couldn't find $am_gamelist, aborting" >&2
  exit 1
fi

# First find the emulator configuration to get some infos
# get the short system name + rompath + extensions
extract_emulator_data


# Initiate the gamelist file
echo "<?xml version='1.0'?><gameList></gameList>" > "$tmp_gamelist"
#echo '<?xml version="1.0"?>' > "$tmp_gamelist"

while IFS=';' read -r Name Title Emulator CloneOf Year Manufacturer Category Players Rotation Control Status DisplayCount DisplayType AltRomname AltTitle Extra Buttons Series Language Region Rating
do
  [[ $Name =~ ^# || -z $Name ]] && continue
  romfile="$(find_real_rom "$Name")" || continue
  desc="$(get_game_description "$Name")"
  video="$(get_scraped_data video "$Name")"
  image="$(get_scraped_data snap "$Name")"
  marquee="$(get_scraped_data marquee "$Name")"

  #echo "$Name === and === $Title"
  desc="$(xml_recode "$desc")"
  Name="$(xml_recode "$Name")"
  Title="$(xml_recode "$Title")"
  romfile="$(xml_recode "$romfile")"
  video="$(xml_recode "$video")"
  image="$(xml_recode "$image")"
  marquee="$(xml_recode "$marquee")"

  # Check for scraped data in the AM as well as media folders
  # Check for the overview in attract/scraper/<system>
  xmlstarlet ed --inplace \
    --subnode /gameList \
    --type elem --name "game" \
    --var new_node '$prev' \
    --subnode '$new_node' --type elem --name 'path' --value "$romfile" \
    --subnode '$new_node' --type elem --name 'name' --value "$Title" \
    --subnode '$new_node' --type elem --name 'desc' --value "$desc" \
    --subnode '$new_node' --type elem --name 'image' --value "$image" \
    --subnode '$new_node' --type elem --name 'video' --value "$video" \
    --subnode '$new_node' --type elem --name 'marquee' --value "$marquee" \
    --subnode '$new_node' --type elem --name 'publisher' --value "$(xml_recode "$Manufacturer")" \
    --subnode '$new_node' --type elem --name 'playcount' --value "$DisplayCount" \
    --subnode '$new_node' --type elem --name 'genre' --value "$Category" \
    --subnode '$new_node' --type elem --name 'players' --value "$Players" \
    "$tmp_gamelist"
done < "$am_gamelist"


# Copy the generated file
install -Dm644 "$tmp_gamelist" "$GA_FRONTENDS"/emulationstation/gamelists/${_system_}/gamelist.xml
# Final cleanings
#cat "$tmp_gamelist"
#rm "$tmp_gamelist"
