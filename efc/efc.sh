#!/bin/bash
# EFC : Emulator Frontend Configuration

source /opt/gatools/include/includes.sh

# ES options
valid_opts_emulationstation=(name fullname extension command platform theme)
# AM options
valid_opts_attract=(filename rompath romext executable args workdir system artwork info_source import_extras)
# Pegasus options
#Damn, variable names can't have a hyphen in their names
#valid_opts_pegasus-frontend=(collection shortname extensions launch)
# Possible artworks
valid_artworks=(flyer marquee snap wheel fanart)

# Expected options in a .efc file
efc_opts=(system extensions path emulator info_source)
# Valid frontends
valid_frontends=(attract emulationstation pegasus-frontend)

declare -A system_hacks
system_hacks["mame"]=arcade

# Source: https://stackoverflow.com/a/8574392
is_valid_option() {
  local e match="$1"
  shift
  for e ; do [[ "$e" == "$match" ]] && return 0 ; done
  return 1
}


# The seperator is a single char, which canbe annoying
join_by() {
  local IFS="$1"
  shift
  echo "$*"
}

join_no_dot() {
  local d=${1-} f=${2-}
  if shift 2 ; then
    printf %s "$f" "${@/#/$d}"
  fi
}

join_with_dot() {
  echo ".$2" | sed "s+ +${1}.+g"
}


tests() {
  is_valid_option flyer "${valid_opts[@]}" && echo "YES" ||echo "nope"
  is_valid_option "flyer" "${valid_artworks[@]}" && echo "YES" ||echo "nope"
}


read_vars_from_file() {
  _fullname_="$(basename "$input_file" .efc)"
  # The eval part is a little ugly, but bash can't read arrays when the array name has a variable in it
  for v in ${efc_opts[@]} $(eval echo \${valid_opts_$fe[@]}) ; do
    tmp="$(grep "^${v}=" "$input_file" | cut -d '=' -f2)"
    eval "_${v}_"='$tmp'
  done
}


configure_emulator_attract() {
  # Better use a temp file, then install -m 644 -o "$GA_USER" -g "$GA_GROUP"
  local tmp_file=/tmp/"${_fullname_}".cfg
  local dest_file="$GA_FRONTENDS"/attract/emulators/"${_fullname_}".cfg
  echo "$dest_file"
  #cat <<EOF
  cat <<EOF > "$tmp_file"
executable           /opt/galauncher/galauncher.sh
args                 ${_emulator_} [system] "[romfilename]"
workdir              \$HOME
rompath              \$HOME/shared/roms/${_system_}/
romext               $(join_with_dot ';' "${_extensions_}")
system               ${_system_}
info_source          ${_info_source_}
import_extras        ${_import_extras_}
artwork    flyer           \$HOME/shared/media/${_system_}/flyer
artwork    marquee         \$HOME/shared/media/${_system_}/marquee
artwork    snap            \$HOME/shared/media/${_system_}/snap;\$HOME/shared/media/${_system_}/video
artwork    wheel           \$HOME/shared/media/${_system_}/wheel
EOF
  install -Dm 644 -o "$GA_USER" -g "$GA_GROUP" "$tmp_file" "$dest_file" && rm "$tmp_file"
}


configure_emulator_emulationstation() {
  local dest_file="$GA_FRONTENDS"/emulationstation/es_systems.cfg

  # Preliminary checks on dir and file existence
  [[ ! -d "$GA_FRONTENDS"/emulationstation ]] && install -m 755 -o "$GA_USER" -g "$GA_GROUP" "$GA_FRONTENDS"/emulationstation
  [[ ! -e "$dest_file" ]] && cat <<EOF > "$dest_file"
<?xml version="1.0"?>
<systemList/>
EOF

  # Now check if the system wasn't added yet
  xmllint --xpath "/systemList/system/fullname[text() = '${_fullname_}']" "$dest_file" &>/dev/null && return 0

  local platform="${_system_}"
  [[ ! -z ${system_hacks["${_system_}"]} ]] && platform="${system_hacks["${_system_}"]}"

  xmlstarlet ed \
    --inplace \
    --subnode /systemList \
    --type elem --name "system" \
    --var new_node '$prev' \
    --subnode '$new_node' --type elem --name 'name' --value "${_system_}" \
    --subnode '$new_node' --type elem --name 'fullname' --value "${_fullname_}" \
    --subnode '$new_node' --type elem --name 'path' --value "/home/arcade/shared/roms/${_system_}" \
    --subnode '$new_node' --type elem --name 'extension' --value "$(join_with_dot ' ' "${_extensions_}")" \
    --subnode '$new_node' --type elem --name 'command' --value "/opt/galauncher/galauncher.sh ${_emulator_} ${_system_} %ROM%" \
    --subnode '$new_node' --type elem --name 'platform' --value "${platform}" \
    --subnode '$new_node' --type elem --name 'theme' --value "${_system_}" \
    "$dest_file"
}


configure_emulator_pegasus-frontend() {
  local dest_file="/home/arcade/shared/roms/${_system_}"/metadata.pegasus.txt

  #cat <<EOF > "$dest_file"
  cat <<EOF
collection: ${_fullname_}
shortname: ${_system_}
extensions: $(join_no_dot ', ' ${_extensions_})
launch: /opt/gatools/galauncher.sh ${_emulator_} ${_system_} {file.path}
EOF
}


fe="$1"
# Check the emulator is valid
if ! is_valid_option "$fe" "${valid_frontends[@]}" ; then
  log_ko "$fe is not a valid frontend, aborting"
  echo "$fe is not a valid frontend, aborting"
  exit 1
fi

# Check the config file exists
input_file="$2"
if [[ ! -e $input_file ]] ; then
  log_ko "$input_file doesn't exist, aborting"
  echo "$input_file doesn't exist, aborting"
  exit 2
fi

read_vars_from_file
configure_emulator_$fe
